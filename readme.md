# DOCKER LARAVEL NGINX MYSQL

## Crear aplicación

- Limpiar o crear carpeta src
- cd src

```composer
composer create-project laravel/laravel . 6.2
```

## config ".env"

```.env
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

## Crear máquinas virtuales

```docker
docker-compose build && docker-compose up
```

## Migrar las tablas

```docker
# Migrar las tablas
docker-compose exec php php /var/www/html/artisan migrate
```

## Eliminar máquinas

```docker
# Eliminar los contenedores del proyecto
docker-compose down
# Eliminar la imagen del proyecto
docker rmi imageId
```

## Dar permisos a bootstrap y storage

- Entrar a la máquina de nginx

```docker
# elegir containerId de nginx
docker ps
# Entrar a la máquina
docker exec -it containerId bash | docker exec -it containerId bin/sh
# Ir a la ruta de la aplicación
cd /var/www/html
# Dar permisos a bootstrap y storage
sudo chown -R $USER:www-data bootstrap/ storage/ | chown -R $USER:www-data bootstrap/ storage/
# Salir
exit
```
